from flask import Flask
from flask_restful import reqparse, Resource, Api
from roman import roman_calculator as rc
from roman import roman_number as rn
from werkzeug.exceptions import BadRequest
import sys

app = Flask(__name__)
api = Api(app)


class RomanExpressionApi(Resource):

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('expression', required=True, type=str)
        args = parser.parse_args()
        try:
            out = rc.RomanExpression(args['expression'])
        except ValueError as e:
            raise BadRequest(e)
        return {'input_expression': args['expression'],
                'result': str(out),
                'result_decimal': out.value}


class RomanToDecimal(Resource):

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('roman', required=True, type=str)
        args = parser.parse_args()
        try:
            out = rn.RomanNumber(args['roman'])
        except ValueError as e:
            raise BadRequest(e)
        return {'input_roman': args['roman'],
                'result_decimal': out.value}


class DecimalToRoman(Resource):

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('decimal', required=True, type=int)
        args = parser.parse_args()
        try:
            out = rn.RomanNumber('I').to_roman(args['decimal'])
        except ValueError as e:
            raise BadRequest(e)
        return {'input_decimal': args['decimal'],
                'result_roman': out}


api.add_resource(RomanExpressionApi, '/roman_expr')
api.add_resource(RomanToDecimal, '/roman_to_decimal')
api.add_resource(DecimalToRoman, '/decimal_to_roman')


if __name__ == '__main__':
    debug = False
    host = '0.0.0.0'
    if 'debug' in sys.argv:
        host = None
        debug = True
    app.run(host=host, debug=debug)