from roman import roman_number as RN
from roman import roman_calculator as RC
import sys

def main():
    """This program takes 2 arguments: command and value and performs conversion to and from roman numerals

    Usage:
    python3 main_program.py [command] value

    command:
        from_roman (default)
        to_roman
        roman_expr

    value: if to_roman command is used

    Examples:
        python3 main_program.py XIX
            converts roman numeral XIX to decimal

        python3 main_program.py to_roman 99
            converts decimal 99 to roman numerals
    """
    narg = len(sys.argv)
    if narg < 2 or narg > 3:
        print('Error: 1 or 2 parameters are expected. See usage inside this script.')
        exit(1)
    if narg == 2:
        value = sys.argv[1]
        command = 'to_roman'
    else:
        value = sys.argv[2]
        command = sys.argv[1]
    if command not in ['to_roman', 'from_roman', 'roman_expr']:
        print('Invalid command %s used. See usage inside this script.' % command)
        exit(1)
    if command == 'roman_expr':
        try:
            out = str(RC.RomanExpression(value))
        except ValueError as e:
            print(e)
            exit(1)
        print("%s = %s" % (value, out))
    if command == 'from_roman':
        try:
            out = RN.RomanNumber(value).value
        except ValueError as e:
            print(e)
            exit(1)
        print("Decimal equivalent: %i" % out)
    if command == 'to_roman':
        try:
            value = int(value)
        except ValueError:
            print('Error: only integer can be converted to roman numeral')
            exit(1)
        try:
            out = RN.RomanNumber('I').to_roman(value)
        except ValueError as e:
            print(e)
            exit(1)
        print("Roman numeral equivalent: %s" % out)



if __name__ == "__main__":
    main()