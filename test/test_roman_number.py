import pytest
from roman import roman_number as RN


class TestRomanNumber():
    numerals = ['M', 'D', 'C', 'L', 'X', 'V', 'I']

    def test_symbols(self):
        assert RN.RomanNumber('I').value == 1
        assert RN.RomanNumber('V').value == 5
        assert RN.RomanNumber('X').value == 10
        assert RN.RomanNumber('L').value == 50
        assert RN.RomanNumber('C').value == 100
        assert RN.RomanNumber('D').value == 500
        assert RN.RomanNumber('M').value == 1000

    def test_simple_addition(self):
        test_number = 'XVII'
        assert RN.RomanNumber(test_number).value == 17

    def test_simple_substraction(self):
        test_number = 'XIV'
        assert RN.RomanNumber(test_number).value == 14

    def test_invalid_roman(self):
        test_number = ['XIIV', 'XVX', 'MMMMMC']
        # test_number = ['MMMMMC']
        for tn in test_number:
            with pytest.raises(ValueError) as err:
                r = RN.RomanNumber(tn)

    def test_invalid_roman_characters(self):
        test_number = 'XXh'
        with pytest.raises(ValueError) as err:
            r = RN.RomanNumber(test_number)

    def test_to_roman_simple(self):
        for n in self.numerals:
            assert RN.RomanNumber(n).to_roman() == n
        # check that it is not just an identity function by calling method with parameter
        assert RN.RomanNumber('I').to_roman(19) == 'XIX'

    def test_to_roman_subtr_notation(self):
        numbers = [(900, 'CM'), (400, 'CD'), (90, 'XC'), (40, 'XL'), (9, 'IX'), (4, 'IV')]
        # just a class to use
        rom = RN.RomanNumber('I')
        for n, r in numbers:
            assert rom.to_roman(n) == r

    def test_to_roman_toolarge(self):
        rom = RN.RomanNumber('I')
        with pytest.raises(ValueError) as err:
            r = rom.to_roman(5000)

    def test_to_roman_sparse(self):
        numbers = [(999, 'CMXCIX'), (494, 'CDXCIV'), (56, 'LVI'), (42, 'XLII'), (7, 'VII'), (4, 'IV')]
        # just a class to use
        rom = RN.RomanNumber('I')
        for n, r in numbers:
            assert rom.to_roman(n) == r

