import pytest
from roman import roman_calculator as RC


class TestRomanCalculator():

    def test_addition(self):
        expr = "XXIV + XI"
        assert RC.RomanExpression(expr).value == 35

    def test_subtraction(self):
        expr = "XXIV - XI"
        assert RC.RomanExpression(expr).value == 13

    def test_division(self):
        expr = "XXXV / V"
        assert RC.RomanExpression(expr).value == 7

    def test_multiplication(self):
        expr = "V * III"
        assert RC.RomanExpression(expr).value == 15

    def test_power(self):
        expr = "IV ^ III"
        assert RC.RomanExpression(expr).value == 64

    def test_precedence_dma(self):
        expr = "IV * V / II + VII"
        assert RC.RomanExpression(expr).value == 17

    def test_precedence_oms(self):
        expr = "IV ^ II * II - VII"
        assert RC.RomanExpression(expr).value == 25

    def test_consecutive_ops(self):
        expr = "V * - III"
        with pytest.raises(ValueError) as err:
            value = RC.RomanExpression(expr).value

    def test_brackets_simple(self):
        expr = "(IV + II) * II"
        assert RC.RomanExpression(expr).value == 12

    def test_brackets_nested(self):
        expr = "((IV + II) * II) / IV"
        assert RC.RomanExpression(expr).value == 3

    def test_unary_negation(self):
        expr = "-VI + X"
        assert RC.RomanExpression(expr).value == 4

    def test_brackets_unpaired(self):
        expr = "(IV + II) * II)"
        with pytest.raises(ValueError) as err:
            value = RC.RomanExpression(expr).value

