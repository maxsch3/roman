from flask import json
from api import app
import pytest


class TestApi:

    @pytest.fixture
    def client(self):
        cli = app.test_client()
        yield cli

    def test_expression_endpoint(self, client):
        response = client.get('/roman_expr?expression=(X%2bII)*III')
        data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 200
        assert 'result' in data
        assert data['result'] == 'XXXVI'

    def test_to_roman_endpoint(self, client):
        response = client.get('/decimal_to_roman?decimal=13')
        data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 200
        assert 'result_roman' in data
        assert data['result_roman'] == 'XIII'

    def test_to_decimal_endpoint(self, client):
        response = client.get('/roman_to_decimal?roman=XI')
        data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 200
        assert 'result_decimal' in data
        assert data['result_decimal'] == 11

    def test_bad_expression(self, client):
        response = client.get('/roman_expr?expression=(X%2bII)*III)')
        #data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 400

    def test_bad_roman(self, client):
        response = client.get('/roman_to_decimal?roman=XID')
        #data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 400

    def test_bad_decimal(self, client):
        response = client.get('/decimal_to_roman?decimal=100000')
        #data = json.loads(response.get_data(as_text=True))
        assert response.status_code == 400



