import numbers
import numpy as np
import re

class RomanNumber(numbers.Number):
    slot = 0
    # dictionaries do not keep order, which I will need below. Using list of tuples
    symbols = [('M', 1000), ('D', 500), ('C', 100), ('L', 50), ('X', 10), ('V', 5), ('I', 1)]
    reverse = [(1000, None, None, 'M'),
               (100, 'M', 'D', 'C'),
               (10,  'C', 'L', 'X'),
               (1,   'X', 'V', 'I')]
    # keeping my code dry by reusing symbols for creating dictionary which I will also need
    symbols_dict = {k: v for k, v in symbols}

    def __init__(self, rn):
        self.slot = self.to_decimal(rn)

    def roman_lookup(self, char):
        try:
            val = self.symbols_dict[char]
        except KeyError:
            raise ValueError('Invalid character provided in roman number. Allowed values are %s' %
                             ''.join(self.symbols_dict.keys()))
        return val


    def to_decimal(self, rn):
        if type(rn) != str:
            raise ValueError('Error: Roman number must be given as a string. Got %s' % type(rn))
        if len(rn) < 1:
            raise ValueError('Error: Empty string provided as roman number')
        # Check for 4+ adjacent repeated characters
        if max([len(x) for x in [re.findall(r'[%s]{4,}' % ch, rn) for ch in self.symbols_dict.keys()]]) > 0:
            raise ValueError('Error: More than 4 characters repeated in a row')
        # Convert all characters in roman number to its decimal representation
        decimal_components = [self.roman_lookup(c) for c in rn]
        # Below vector code works but lacks of validity verification I will use for loop with error checks
        # # check for subtractive notation
        # # I'm using vector operations below rather than naive for loop. Vector operations are much faster.
        # ## first create a list of next element made by shifting decimal_components left and adding 0 at the end
        # next_element = decimal_components[1:] + [0]
        # ## then, loop through two lists comparing current and next
        # decimal_components = [cur if cur >= nxt else -cur for cur, nxt in zip(decimal_components, next_element)]

        # For loop variant with error checks embedded
        prev = 0
        magnitude = 0
        sum = 0
        for i, d in enumerate(decimal_components[::-1]):
            if d >= magnitude:
                magnitude = d
                sum += d
            else:   # probably subtractive notation
                if prev < magnitude: # second digit with lower magnitude -
                    raise ValueError('Error: Left to right magnitude decrease not maintained')
                if d not in [1, 10, 100]:
                    raise ValueError('Error: Invalid subtractive notation. Only I, X or C can be used')
                sum -= d
            prev = d
        return sum

    def to_roman(self, num=None):
        # in this instance for loop is appropriate
        remainder = 0
        if num is not None:
            if type(num) != int:
                raise ValueError('Error: Only integer can be converted to roman numeral. Got %s' % type(num))
            if num > 3999:
                raise ValueError('Error: Only integer < 4000 can be converted to roman numeral. Got %s' % str(num))
            remainder = num
        else:
            remainder = self.slot
        roman = ''
        for i, _ in enumerate(self.reverse):
            num = int(remainder // self.reverse[i][0])
            if num > 0:
                roman = ''.join([roman, self._digit2roman(num, self.reverse[i])])
            remainder -= self.reverse[i][0] * num
            if remainder == 0:
                break
        return roman

    def _digit2roman(self, digit, numerals):
        if digit == 9:
            return ''.join([numerals[3], numerals[1]])
        if digit > 5:
            return ''.join([numerals[2]] + [numerals[3]]*(digit-5))
        if digit == 5:
            return numerals[2]
        if digit == 4:
            return ''.join([numerals[3], numerals[2]])
        if digit >= 1:
            return ''.join([numerals[3]]*digit)
        return ''


    @property
    def value(self):
        return self.slot
