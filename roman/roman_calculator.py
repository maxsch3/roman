from roman import roman_number as RN
import re


class RomanExpression:
    """
    This class will implement parsing and calculation of expressions based on roman numerals
    E.g.

    It is using safe evaluation
    """

    ops_resolution = [r'\^', r'*/', r'+\-']
    op_functions = {}

    value = None

    def __init__(self, expr):
        if type(expr) not in [str, list]:
            raise ValueError('Error: only lists and strings are allowed in parser. Got %s' % type(expr))
        # expr = "(IV + II) * II"
        self.op_functions = {'^': self._power,
                             '*': self._multiply,
                             '/': self._divide,
                             '+': self._add,
                             '-': self._subtract}
        self.value = self.parse(expr)

    def tokenize(self, expr):
        # do not treat brackets for now
        # Remove all whitespaces
        expr = re.sub(r'\s+', '', expr)
        # check that there are no two or more adjacent operators
        if re.search(r'[\^*/+\-]{2,}', expr):
            raise ValueError('Error: More than 2 consecutive operator symbols not allowed')
        # I will need to split by operators keeping them as separate element in resulting list. Python does not support
        # zero length split, therefore I'm adding spaces before and after any operator and then split string by spaces.
        # It is safe because spaces have been removed previously
        expr = re.sub(r'([()\^*/+\-])', r' \1 ', expr).split(' ')
        # if unary operator is in the beginning of string like "-II + III" above split will produce empty element
        expr = [e for e in expr if e]
        if '(' in expr or ')' in expr:
            expr = self.solve_brackets(expr)
        return expr

    def parse(self, expr):
        if type(expr) == list:
            if len(expr) == 1:
                expr = expr[0]
        if type(expr) == str:
            if re.search(r'[()*/\^+\-]+', expr):
                expr = self.tokenize(expr)
                return self.parse(expr)
            else:
                return RN.RomanNumber(expr).value
        else:
            # the expression is a tokenized list. I will only contain numerals and operators without brackets,
            # which are solved before, in tokenize function
            # first, find the top operator that takes precedence. Bear in mind * and / have same precedence as well as
            # + and -. Therefore I have to use * or / type of matching to find top op. This means I cannot use
            # simple list.index() which only supports exact match. I will use regexp and charater classes for that
            # It came out that in recursive mode operators have to parsed in reverse order from right to left.
            # This is because the algorithm first calculates values at deepest levels of recursion and then it combines
            # these values at higher levels. Therefore highest priority operators have to be parsed last.
            indices = [[i for i, val in enumerate(expr) if self._list_friendly_match(re.compile(r'[%s]' % op), val)]
                       for op in self.ops_resolution[::-1]]
            # indices holds indexes of operators found ordered in operator precedence order. Ex: "II + II * III - II"
            # will translate to [[1, 5], [3], []] meaning [+-] = two found at positions 1 and 5,
            # [*/] = one found at position 3, power operators = None found,
            # Next, top priority operators should be selected. It will be first non-empty element of indices list
            top_ops = next((i for i, x in enumerate(indices) if x), None)
            if top_ops is None:
                raise ValueError("Error: no operation between two values. ")
            # Now, since we are parsing in reverse order, last operator in the first non-empty group will be selected
            top_op_idx = indices[top_ops][-1]
            top_operator = expr[top_op_idx]
            try:
                return self.op_functions[top_operator](expr, top_op_idx)
            except KeyError:
                raise ValueError("Error: invalid operator '%s' used" % top_operator)

    def solve_brackets(self, expr):
        """This function will parse expression like '(III + II) * V' to a list [['III + II'], '* V']
        Based on the code from here: https://stackoverflow.com/a/50702934
        Im using this code without modifications, but unlike original, I'm using it on tokenized expression rather than
        on strings
        """
        def push(obj, l, depth):
            while depth:
                l = l[-1]
                depth -= 1
            l.append(obj)

        groups = []
        depth = 0
        try:
            for e in expr:
                if e == '(':
                    push([], groups, depth)
                    depth += 1
                elif e == ')':
                    depth -= 1
                else:
                    push(e, groups, depth)
        except IndexError:
            raise ValueError('Parentheses mismatch')

        if depth != 0:
            raise ValueError('Parentheses mismatch')
        else:
            return groups

    def _power(self, expr, op_index):
        split = self._prepare_binary_op(expr, op_index, 'power')
        return self.parse(split[0]) ** self.parse(split[1])

    def _multiply(self, expr, op_index):
        split = self._prepare_binary_op(expr, op_index, 'multiplication')
        return self.parse(split[0]) * self.parse(split[1])

    def _divide(self, expr, op_index):
        split = self._prepare_binary_op(expr, op_index, 'division')
        return self.parse(split[0]) / self.parse(split[1])

    def _add(self, expr, op_index):
        split = self._prepare_binary_op(expr, op_index, 'addition')
        return self.parse(split[0]) + self.parse(split[1])

    def _subtract(self, expr, op_index):
        if op_index == 0:
            # Unary negation
            if len(expr) != 2:
                raise ValueError("Error: parsing error: unary operators must be parsed after all others. "
                                 "Parsed expression must include exactly 2 tokens. Got %s" % len(expr))
            return self.parse(expr[1]) * -1
        else:
            split = (expr[:op_index], expr[(op_index+1):])
            return self.parse(split[0]) - self.parse(split[1])

    def _prepare_binary_op(self, expr, op_index, operation):
        if op_index == 0:
            raise ValueError("Error: %s operator is used as unary operator" % operation)
        if op_index == len(expr):
            raise ValueError("Error: %s operator is used without second value" % operation)
        return (expr[:op_index], expr[(op_index+1):])

    def __eq__(self, other):
        if type(other) == int:
            return self.value == other
        if type(other) == type(self):
            return self.value == other.value
        return False

    def _list_friendly_match(self, r, expr):
        if type(expr) == str:
            return r.match(expr)
        else:
            return False

    def __str__(self):
        """Print out result as roman numeral"""
        return RN.RomanNumber('I').to_roman(self.value)