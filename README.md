# Roman calculator

This is a program for converting roman numerals to decimals and decimals and decimals to roman

# Installation

Clone repository

```shell
git clone git@bitbucket.org:maxsch3/roman.git
```
create a virtual environment

```shell
cd Roman
python3 -m venv venv
```

activate new environment
```shell
source venv/bin/activate
```

install dependencies

```shell
pip install -r requirements.txt
```
You are now ready to use the program

## Building a docker image 

build a docker image using below command

```shell
docker build . -t roman
```

# Usage

## Command line

```shell
python3 main_program.py [command] value
```

Command takes the following values:

`to_roman` - convert decimal value to roman numeral. Only accepts integer as `value`

`from_roman` - converts roman numeral to integer 

`roman_expr` - calculates result and prints it back as roman numeral. Fully supports all BODMAS calculation

## web API

You can run the web API in two ways:

### Running API as local python program

```shell
python3 main_program.py [debug]
```

Optional arguments:

`debug` - the program is run in debug (development) mode when it only accepts connections from localhost and restarts automatically when any of source files is changed

use this option in your development environment

### Running API as docker container

Instantiate a docker container

```shell
docker run -p 5000:5000 roman
```
and test API in your local browser by navigating the [link](http://localhost:5000/roman_expr?expression=II*III)

## API endpoints

The API provides 3 endpoints:

`/roman_expr` - for evaluating BODMAS expression written in roman numerals. It returns result as JSON and includes results of evaluation in both decimal and roman representation. *Mandatory parameters*: this endpoint requires one mandatory parameter: `expression`

`/roman_to_decimal` - endpoint for converting roman numeral to decimal representation. *Mandatory parameters*: this endpoint requires one mandatory parameter: `roman`

`/decimal_to_roman` - endpoint for converting decimal number to roman numeral. *Mandatory parameters*: this endpoint requires one mandatory parameter: `decimal`

all above API endpoints accept only GET requests in format:

```shell
http://address:port/endpoint?variable=value
```

### Examples of API calls and responses

####Evaluating roman expression

Get request
```shell
http://localhost:5000/roman_expr?expression=II*III
```

Response:
```json
{"input_expression": "II*III", "result": "VI", "result_decimal": 6}
```

####Converting decimal to roman

Get request

```shell
http://localhost:5000/decimal_to_roman?decimal=16
```

Response

```json
{"input_decimal": 16, "result_roman": "XVI"}
```

#### Converting roman to decimal

Get request

```shell
http://localhost:5000/roman_to_decimal?roman=XIX
```

Response

```json
{"input_roman": "XIX", "result_decimal": 19}
```

