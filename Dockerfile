FROM python:3.7-alpine

WORKDIR /usr/src/app
COPY requirements.txt ./

EXPOSE 5000

RUN apk --no-cache add musl-dev linux-headers g++

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./api.py" ]